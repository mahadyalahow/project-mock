# Project-mock


## Beschreibung

Ziel des Projekts ist es, ein bestehendes webbasiertes Projekt in Helm Charts zu übertragen, indem unabhängige Charts erstellt werden, die als Abhängigkeiten eines Hauptcharts dienen. Jedes Teammitglied soll eine Teilaufgabe übernehmen und in Form von Tickets in Feature Branches bearbeiten. Die Feature Branches sollen dann in einem nachvollziehbaren Review-Prozess per Merge Request in zentrale Branches für mehrere Profile, wie z.B. dev, prod, stage, usw. gemerged werden. Die Aufgabe beinhaltet die Verwendung einer vorhandenen GitLab-Instanz und die Einrichtung eines bereits verfügbaren Kubernetes-Clusters, der noch um alles notwendige erweitert werden muss.


Teilaufgaben:

- Einrichtung eines Kubernetes-Clusters, einschließlich der Installation aller notwendigen Komponenten wie kubectl, kubelet, kubeadm, usw.
- Erstellung eines Charts für die Datenbank, einschließlich einer Skriptdatei zum Erstellen und Initialisieren der Datenbanktabellen. 
- Die Persistenzschicht muss redundant ausgelegt werden.
- Erstellung eines Charts für den Backend-Service, einschließlich Konfigurationsdateien für die Umgebungsvariablen und der Erstellung von Kubernetes-Objekten wie Deployments und Services. Dieses Chart muss idempotent sein.
- Erstellung eines Charts für den Frontend-Service, einschließlich Konfigurationsdateien für die Umgebungsvariablen und der Erstellung von Kubernetes-Objekten wie Deployments und Services. Dieses Chart muss idempotent sein.
- Erstellung eines Charts für das API-Gateway, einschließlich Konfigurationsdateien für die Umgebungsvariablen und der Erstellung von Kubernetes-Objekten wie Deployments und Services. Dieses Chart muss idempotent sein.
- Erstellung eines Charts für den WebSocket-Service, einschließlich Konfigurationsdateien für die Umgebungsvariablen und der Erstellung von Kubernetes-Objekten wie Deployments und Services. Dieses Chart muss idempotent sein.
- Erstellung einer umfassenden Dokumentation, die die Verwendung der Helm-Charts und die Schritte zum Bereitstellen des Projekts im Kubernetes-Cluster erläutert.

Abschlussaufgabe:
Zusammenführen aller Teilaufgaben in einem Hauptchart, das alle anderen Charts als Abhängigkeiten referenziert und die erforderlichen Kubernetes-Objekte wie Deployments und Services erstellt. Außerdem sollte eine Ingress-Regel hinzugefügt werden, um den Zugriff auf den Webdienst von außerhalb des Kubernetes-Clusters zu ermöglichen. Das fertige Helm-Chart sollte in der vorhandenen GitLab-Instanz bereitgestellt werden. Der gesamte Prozess sollte idempotent sein, um die Wiederholbarkeit und Konsistenz zu gewährleisten. Alle Komponenten sollten im Kubernetes-Cluster vorhanden und voll funktionsfähig sein. Die Feature-Branches sollten in einem nachvollziehbaren Review-Prozess per Merge Request in zentrale Branches für mehrere Profile, wie z.B. der prod, stage, gemerged werden.
